Your self-signed certificate has been generated. Download the files below and store in a folder reachable by the web server, for example /etc/apache2/ssl.

Apache Configuration
You need the following ssl configuration in your VirtualHost:

<VirtualHost redirect.samplebank.com:443>
	ServerName redirect.samplebank.com
	
	SSLEngine on
	SSLCertificateKeyFile /etc/apache2/ssl/redirect.samplebank.com.key
	SSLCertificateFile /etc/apache2/ssl/redirect.samplebank.com.cert
	SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown

	# ...
</VirtualHost>
You will also need all the settings and sections required for a regular HTTP host, such as DocumentRoot and <Directory>. You must also enable apache to listen on port 443, which is done using the directive Listen 443.

How to repeat
You can create a key and certificate yourself instead of downloading them from this page. This makes your key more secure. To generate a key:

openssl genrsa -out redirect.samplebank.com.key 2048

And the certificate:

openssl req -new -x509 -key redirect.samplebank.com.key -out redirect.samplebank.com.cert -days 3650 -subj /CN=redirect.samplebank.com