﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Cryptography.X509Certificates;

namespace SampleApi.Controllers
{
    public class RedirectController : ApiController
    {
        [HttpGet]
        //[Authorize]
        public string Get()
        {
            X509Certificate2 cs = Request.GetClientCertificate();
            var subject = cs.Subject;
            return "Authenticated";
        }

        [HttpGet]
        public string Get(int id)
        {
            return "Public method : " + id;
        }
    }
}